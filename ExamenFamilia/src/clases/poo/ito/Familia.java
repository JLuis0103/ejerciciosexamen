package clases.poo.ito;

import java.util.ArrayList;

public class Familia {

	private String padre;
	private String madre;
	private ArrayList<String> hijos = new ArrayList<String>();
	private static int numeroFamilias = 0;
	
	public Familia(String padre, String madre) {
		super();
		this.padre = padre;
		this.madre = madre;
		numeroFamilias++;
	}
	
	
	//=========================================
	
	public String getPadre() {
		return padre;
	}
	
	public void setPadre(String padre) {
		this.padre = padre;
	}
	
	public String getMadre() {
		return madre;
	}
	
	public void setMadre(String madre) {
		this.madre = madre;
	}
	
	public ArrayList<String> getHijos() {
		return hijos;
	}
	
	public void addHijos(String Hijos) {
		boolean c = false;
		for (int i=0; i < this.hijos.size(); i++) 
			if (Hijos.equals(this.hijos.get(i)))
				c=true;
		if (c == false)
			this.hijos.add(Hijos);			
	}

	public static int getnumeroFamilias() {
		return numeroFamilias;
	}
	
	
	//=========================================

	@Override
	public String toString() {
		return "Familia [padre=" + padre + ", madre=" + madre + ", hijos=" + hijos + "]";
	}
	

}