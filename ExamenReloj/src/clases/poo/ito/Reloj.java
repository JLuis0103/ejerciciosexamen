package clases.poo.ito;

public class Reloj {
	
	private int hora = 0;
	private int minutos = 0;
	private int segundos = 0;
	
	//=============================
	
	public Reloj() {
		super();
	}

	public Reloj(int hora, int minutos, int segundos) {
		super();
		this.hora = hora;
		this.minutos = minutos;
		this.segundos = segundos;
	}
	
	//=============================

	public int getHora() {
		return hora;
	}
	
	public int getMinutos() {
		return minutos;
	}

	public int getSegundos() {
		return segundos;
	}
	
	public void incrementaSegundos(int Segundos) {
		this.segundos = this.segundos + Segundos;
		if (this.segundos > 60) 
			do {
				this.segundos = this.segundos - 60;
				this.minutos = this.minutos + 1;
			} while (this.segundos > 60);
			
	
		if (this.minutos > 60) {
			do {
				this.minutos = this.minutos - 60;
				this.hora = this.hora + 1;
			} while (this.minutos > 60);
		}
		
		if (this.hora > 24) 
			do {
				this.hora = this.hora - 24;
			} while (this.hora > 24);
	}
	
	//=============================
	
	@Override
	public String toString() {
		return "Reloj [Horas: " + hora + ", Minutos: " + minutos + ", Segundos: " + segundos + "]";
	}

}
