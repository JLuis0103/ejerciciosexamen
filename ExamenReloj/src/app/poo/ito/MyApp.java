package app.poo.ito;
import clases.poo.ito.Reloj;

public class MyApp {
	
	static void run() {
		Reloj r1 = new Reloj();
		
		r1.incrementaSegundos(125);
		System.out.println(r1);
		
		r1.incrementaSegundos(1125);
		System.out.println(r1);
		
		r1.incrementaSegundos(11125);
		System.out.println(r1);
	}

	public static void main(String[] args) {
		run();
	}

}
